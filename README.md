# LizaWord

## Descripción del Juego

Tenemos un círculo de lagartijas alrededor de una poza de agua. Al hacer click. sobre cada lagartija, aparecerán letras aparentemente al azar (cada lagartija tendrá su grupo asignado de letras). El jugador debe hacer click sobre las lagartijas para formar palabras. Cuando el jugador encuentre la palabra que ocultan las lagartijas, ellas cantarán. Mientras está buscando las letras adecuadas, las lagartijas harán sonidos raros.

## Contexto Women Game Jam (WGJ)

Este juego fue elaborado en el contexto de la WGJ 2020 Online. La temática de este evento es "Juntas en la Distancia". La concordancia con esta temática se llevará a cabo en el juego de palabras: las palabras que aparecerán durante el juego fueron inspiradas en esta temática.

## Juego de palabras

No se utilizarán palabras textuales de la frase "Juntas en la Distancia" porque sería muy obvio y fácil jugar. Por temas de espacio (cuántas lagartijas clickeables caben en una pantalla) vamos a preferir palabras no muy largas como "Distancia" y no tan cortas como "en", "la".

Inicialmente se van a preferir palabras de 4 a 6 letras. El largo de la palabra por el momento no tiene relación con la dificultad del juego.

Se considerarán palabras específicas en inglés y español. Consideraremos añadir portugués por el contexto americano.

## Documentación

Las lagartijas del juego fueron inspiradas en lagartijas chilenas. Pueden ver las fotos usadas en los siguientes enlaces:

[Wikipedia](https://upload.wikimedia.org/wikipedia/commons/f/fd/Liolaemus_lemniscatus.jpg "Liolaemus Lemniscatus"), [Gustavo-z](https://www.gustavo-z.cl/reptiles-de-chile/lagartijas-chilenas/ "Lagartijas Chilenas"), [Wikipedia](https://upload.wikimedia.org/wikipedia/commons/b/b6/Liolaemus_Tenuis.jpg "Liolaemus Tenuis"), [Flora y Fauna Lo Barnechea](http://florayfaunalobarnechea.cl/my-product/lagartija-chilena/ "Lagartija chilena")

## Jugar

[Enlace al juego en Itch.io](https://meowmer.itch.io/lizaword)
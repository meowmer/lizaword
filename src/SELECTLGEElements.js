window.SELECTLGEElements = {
    addLizard: function (scope) {
        scope.lizardSELECTLGE = window.addSomething(scope, 400, 360, 'lizard_SELECTLGE', 0.5, 0.5, 300, 270);
    },
    addGameTitle: function (scope) {
        scope.gameTitle = window.addSomething(scope, 400, 105, 'lizaword_title', 0.5, 0.5, 500, 165);
    },
    addBtnEnglish: function (scope) {
        setX = 237;
        setY = 550;
        scope.btnEnglishLanguage = window.addSomething(scope, setX, setY, 'btn_lge', 0.5, 0.5, 170, 50);
        scope.btnEnglishText = window.addText(scope, setX, setY, 'ENGLISH', '#2a6048', 'center', 0.5, 0.5);
    },
    addBtnSpanish: function (scope) {
        setX = 570;
        setY = 550;
        scope.btnSpanishLanguage = window.addSomething(scope, setX, setY, 'btn_lge', 0.5, 0.5, 170, 50);
        scope.btnSpanishText = window.addText(scope, setX, setY, 'ESPAÑOL', '#2a6048', 'center', 0.5, 0.5);
    },
    addCards: function (scope) {
        scope.cardW1 = window.addSomething(scope, 100, 205, 'word_w1', 0.5, 0.5, 60, 75);
        scope.cardO2 = window.addSomething(scope, 170, 405, 'word_o2', 0.5, 0.5, 60, 75);
        scope.cardR3 = window.addSomething(scope, 685, 225, 'word_r3', 0.5, 0.5, 60, 75);
        scope.cardD4 = window.addSomething(scope, 620, 415, 'word_d4', 0.5, 0.5, 60, 75);
    },
    playSound: function (scope) {
        scope.audioStart.play();
        scope.audioStart.setLoop(true);
    },
    stopSound: function (scope) {
        console.log('stop');
        scope.audioStart.destroy();
    }
}
window.SELECTLGEFunctionality = {
    onBtnEnglish: function (scope) {
        scope.btnEnglishLanguage.setInteractive({ useHandCursor: true }); // btn clickable
        scope.btnEnglishLanguage.on('pointerup', function(pointer, localX, localY, event) {
            window.SELECTLGEElements.stopSound(scope);
            // When we select a language, game start
            scope.state = scope.states.GAME_ENGLISH;
            // When game start, we have to hide the assets of previous state
            window.SELECTLGEFunctionality.hideSELECTLGEElements(scope);
            // Show GAME_ENGLISH elements
            window.GAMEElements.addFirsInstruction(scope);
            window.GAMEElements.addPuddle(scope);
            window.GAMEElements.addLizards(scope);
            window.GAMEElements.addCards(scope);
            window.GAMEFunctionality.animsPuddle(scope);
            window.GAMEFunctionality.animsLizards(scope);
            window.GAMEFunctionality.clickOnLizards(scope);
            const word = window.WordsGameLogic.chooseAWord(window.WordsGameLogic.wordsENGLISH);
            window.WordsGameLogic.hideLizards(scope, word);
            window.WordsGameLogic.createArrayOfLettersPerLetter(scope, word);
        });
    },
    onBtnSpanish: function (scope) {
        scope.btnSpanishLanguage.setInteractive({ useHandCursor: true }); // btn clickable
        scope.btnSpanishLanguage.on('pointerup', function(pointer, localX, localY, event) {
            window.SELECTLGEElements.stopSound(scope);
            // When we select a language, game start
            scope.state = scope.states.GAME_SPANISH;
            // When game start, we have to hide the assets of previous state
            window.SELECTLGEFunctionality.hideSELECTLGEElements(scope);
            // Show GAME_SPANISH elements
            window.GAMEElements.addFirsInstruction(scope);
            window.GAMEElements.addPuddle(scope);
            window.GAMEElements.addLizards(scope);
            window.GAMEElements.addCards(scope);
            window.GAMEFunctionality.animsPuddle(scope);
            window.GAMEFunctionality.animsLizards(scope);
            window.GAMEFunctionality.clickOnLizards(scope);
            const word = window.WordsGameLogic.chooseAWord(window.WordsGameLogic.wordsSPANISH);
            window.WordsGameLogic.hideLizards(scope, word);
            window.WordsGameLogic.createArrayOfLettersPerLetter(scope, word);
        });
    },
    hideSELECTLGEElements: function (scope) {
        scope.lizardSELECTLGE.visible = false;
        scope.btnEnglishLanguage.visible = false;
        scope.btnSpanishLanguage.visible = false;
        scope.btnEnglishText.visible = false;
        scope.btnSpanishText.visible = false;
        scope.gameTitle.visible = false;
        scope.cardD4.visible = false;
        scope.cardO2.visible = false;
        scope.cardR3.visible = false;
        scope.cardW1.visible = false;
    }
}
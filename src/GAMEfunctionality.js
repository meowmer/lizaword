window.GAMEFunctionality = {
    firstClick: false,
    animsLizards: function (scope) {
        scope.lizard1.play('lizard1Anims');
        scope.lizard2.play('lizard2Anims');
        scope.lizard3.play('lizard3Anims');
        scope.lizard4.play('lizard4Anims');
        scope.lizard5.play('lizard5Anims');
        scope.lizard6.play('lizard6Anims');
    },
    animsPuddle: function (scope) {
        scope.puddle.play('puddleAnims');
    },
    afterFirstClick: function (scope) {
        if (!window.GAMEFunctionality.firstClick) {
            scope.firstInstructionSymbol.visible = false;
            scope.firstInstructionText.visible = false;
            window.GAMEElements.addSecondInstruction(scope);
            window.GAMEFunctionality.firstClick = true;
        }
    },
    checkIfPlayerWins: function (scope) {
        const currentWord = (
            scope.currentLetter1 + scope.currentLetter2 + scope.currentLetter3 +
            scope.currentLetter4 + scope.currentLetter5 + scope.currentLetter6
        );
        console.log('currentWord: ', currentWord);
        console.log('gamedWord: ', window.WordsGameLogic.choosedWord);
        if (currentWord === window.WordsGameLogic.choosedWord) {
            // PLAYER WINs
            scope.state = scope.states.WIN;
            console.log('PLAYER WINS');
            scope.audioWin.play();
            // Llamar funcion que elimine cosas de GAME state
            window.WINfunctionality.clearGAMEState(scope);
            // Crear elementos de WIN state
            window.WINElements.addLizards(scope);
            window.WINfunctionality.animsLizards(scope);
            window.WINElements.addGamedWord(scope);
        }
    },
    clickOnLizards: function (scope) {
        scope.lizard1.setInteractive({ useHandCursor: true }); // lizard clickable
        scope.lizard2.setInteractive({ useHandCursor: true }); // lizard clickable
        scope.lizard3.setInteractive({ useHandCursor: true }); // lizard clickable
        scope.lizard4.setInteractive({ useHandCursor: true }); // lizard clickable
        scope.lizard5.setInteractive({ useHandCursor: true }); // lizard clickable
        scope.lizard6.setInteractive({ useHandCursor: true }); // lizard clickable

        // if it choosed word have length = 4, show only lizards 1,2,4,5
        // if it choosed word have length = 5, show only lizards 1,2,4,5,6
        // if it choosed word have length = 6, show all lizards

        scope.lizard1.on('pointerup', function(pointer, localX, localY, event) {
            const lizardIndex = 0;
            scope.currentLetter1 = window.WordsGameLogic.returnALetterFromLizardLettersArray(lizardIndex, scope.currentLetter1);
            scope.card1.visible = true;
            scope.cardLetter1.visible = true;
            scope.cardNumber1.visible = true;
            scope.cardLetter1.setText(scope.currentLetter1);
            scope.cardNumber1.setText((lizardIndex + 1).toString(10));
            scope.lizardSound1.play();
            window.GAMEFunctionality.afterFirstClick(scope);
            window.GAMEFunctionality.checkIfPlayerWins(scope);
        });
        scope.lizard2.on('pointerup', function(pointer, localX, localY, event) {
            const lizardIndex = 1;
            scope.currentLetter2 = window.WordsGameLogic.returnALetterFromLizardLettersArray(lizardIndex, scope.currentLetter2);
            scope.card2.visible = true;
            scope.cardLetter2.visible = true;
            scope.cardNumber2.visible = true;
            scope.cardLetter2.setText(scope.currentLetter2);
            scope.cardNumber2.setText((lizardIndex + 1).toString(10));
            scope.lizardSound1.play();
            window.GAMEFunctionality.afterFirstClick(scope);
            window.GAMEFunctionality.checkIfPlayerWins(scope);
        });
        scope.lizard3.on('pointerup', function(pointer, localX, localY, event) {
            const arrLength = window.WordsGameLogic.lizardsLetters.length;
            if (arrLength === 6) {
                const lizardIndex = 2;
                scope.currentLetter3 = window.WordsGameLogic.returnALetterFromLizardLettersArray(lizardIndex, scope.currentLetter3);
                scope.card3.visible = true;
                scope.cardLetter3.visible = true;
                scope.cardNumber3.visible = true;
                scope.cardLetter3.setText(scope.currentLetter3);
                scope.cardNumber3.setText((lizardIndex + 1).toString(10));
                scope.lizardSound1.play();
                window.GAMEFunctionality.afterFirstClick(scope);
                window.GAMEFunctionality.checkIfPlayerWins(scope);
            }
        });
        scope.lizard4.on('pointerup', function(pointer, localX, localY, event) {
            let lizardIndex = 3;
            const arrLength = window.WordsGameLogic.lizardsLetters.length;
            if ((arrLength === 4) || (arrLength === 5)) {
                lizardIndex = 2;
            }
            scope.currentLetter4 = window.WordsGameLogic.returnALetterFromLizardLettersArray(lizardIndex, scope.currentLetter4);
            scope.card4.visible = true;
            scope.cardLetter4.visible = true;
            scope.cardNumber4.visible = true;
            scope.cardLetter4.setText(scope.currentLetter4);
            scope.cardNumber4.setText((lizardIndex + 1).toString(10));
            scope.lizardSound1.play();
            window.GAMEFunctionality.afterFirstClick(scope);
            window.GAMEFunctionality.checkIfPlayerWins(scope);
        });
        scope.lizard5.on('pointerup', function(pointer, localX, localY, event) {
            let lizardIndex = 4;
            const arrLength = window.WordsGameLogic.lizardsLetters.length;
            if ((arrLength === 4) || (arrLength === 5)) {
                lizardIndex = 3;
            }
            scope.currentLetter5 = window.WordsGameLogic.returnALetterFromLizardLettersArray(lizardIndex, scope.currentLetter5);
            scope.card5.visible = true;
            scope.cardLetter5.visible = true;
            scope.cardNumber5.visible = true;
            scope.cardLetter5.setText(scope.currentLetter5);
            scope.cardNumber5.setText((lizardIndex + 1).toString(10));
            scope.lizardSound1.play();
            window.GAMEFunctionality.afterFirstClick(scope);
            window.GAMEFunctionality.checkIfPlayerWins(scope);
        });
        scope.lizard6.on('pointerup', function(pointer, localX, localY, event) {
            let lizardIndex = 5;
            const arrLength = window.WordsGameLogic.lizardsLetters.length;
            if (arrLength === 5) {
                lizardIndex = 4;
            }
            scope.currentLetter6 = window.WordsGameLogic.returnALetterFromLizardLettersArray(lizardIndex, scope.currentLetter6);
            scope.card6.visible = true;
            scope.cardLetter6.visible = true;
            scope.cardNumber6.visible = true;
            scope.cardLetter6.setText(scope.currentLetter6);
            scope.cardNumber6.setText((lizardIndex + 1).toString(10));
            scope.lizardSound1.play();
            window.GAMEFunctionality.afterFirstClick(scope);
            window.GAMEFunctionality.checkIfPlayerWins(scope);
        });
    }
}
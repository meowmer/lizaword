window.GAMEElements = {
    createLizardAnims: function (scope, lizaNumber) {
        lizaNumber = lizaNumber.toString(10); // transform number to string
        scope.anims.create({
            key: 'lizard'+ lizaNumber + 'Anims',
            frames: [
                { key: 'sprite_lizard_'+ lizaNumber + '0' },
                { key: 'sprite_lizard_'+ lizaNumber + '1', duration: 50 }
            ],
            frameRate: 2,
            repeat: -1
        });
    },
    addLizards: function (scope) {
        window.GAMEElements.createLizardAnims(scope, 6);
        window.GAMEElements.createLizardAnims(scope, 5);
        window.GAMEElements.createLizardAnims(scope, 4);
        window.GAMEElements.createLizardAnims(scope, 3);
        window.GAMEElements.createLizardAnims(scope, 2);
        window.GAMEElements.createLizardAnims(scope, 1);

        scope.lizard6 = window.addSprite(scope, 400, 430, 'sprite_lizard_60', 0.5, 0.5, 113, 120);
        scope.lizard5 = window.addSprite(scope, 620, 410, 'sprite_lizard_50', 0.5, 0.5, 220, 98);
        scope.lizard4 = window.addSprite(scope, 630, 290, 'sprite_lizard_40', 0.5, 0.5, 190, 83);
        scope.lizard3 = window.addSprite(scope, 400, 175, 'sprite_lizard_30', 0.5, 0.5, 113, 120);
        scope.lizard2 = window.addSprite(scope, 180, 250, 'sprite_lizard_20', 0.5, 0.5, 220, 98);
        scope.lizard1 = window.addSprite(scope, 200, 380, 'sprite_lizard_10', 0.5, 0.5, 190, 83);
    },
    addCards: function (scope) {
        scope.card6 = window.addSomething(scope, 340, 530, 'card', 0.5, 0.5, 65, 78);
        scope.card5 = window.addSomething(scope, 610, 510, 'card', 0.5, 0.5, 65, 78);
        scope.card4 = window.addSomething(scope, 610, 180, 'card', 0.5, 0.5, 65, 78);
        scope.card3 = window.addSomething(scope, 340, 105, 'card', 0.5, 0.5, 65, 78);
        scope.card2 = window.addSomething(scope, 130, 170, 'card', 0.5, 0.5, 65, 78);
        scope.card1 = window.addSomething(scope, 130, 480, 'card', 0.5, 0.5, 65, 78);

        scope.card6.visible = false;
        scope.card5.visible = false;
        scope.card4.visible = false;
        scope.card3.visible = false;
        scope.card2.visible = false;
        scope.card1.visible = false;

        scope.cardLetter1 = window.addText(scope, 130, 471, 'X', '#df932e', 'center', 0.5, 0.5, '70px');
        scope.cardLetter2 = window.addText(scope, 130, 161, 'X', '#df932e', 'center', 0.5, 0.5, '70px');
        scope.cardLetter3 = window.addText(scope, 340, 96, 'X', '#df932e', 'center', 0.5, 0.5, '70px');
        scope.cardLetter4 = window.addText(scope, 610, 171, 'X', '#df932e', 'center', 0.5, 0.5, '70px');
        scope.cardLetter5 = window.addText(scope, 610, 501, 'X', '#df932e', 'center', 0.5, 0.5, '70px');
        scope.cardLetter6 = window.addText(scope, 340, 521, 'X', '#df932e', 'center', 0.5, 0.5, '70px');
        scope.cardNumber1 = window.addText(scope, 115, 505, '0', '#df932e', 'center', 0.5, 0.5, '28px');
        scope.cardNumber2 = window.addText(scope, 115, 195, '0', '#df932e', 'center', 0.5, 0.5, '28px');
        scope.cardNumber3 = window.addText(scope, 325, 128, '0', '#df932e', 'center', 0.5, 0.5, '28px');
        scope.cardNumber4 = window.addText(scope, 595, 205, '0', '#df932e', 'center', 0.5, 0.5, '28px');
        scope.cardNumber5 = window.addText(scope, 595, 535, '0', '#df932e', 'center', 0.5, 0.5, '28px');
        scope.cardNumber6 = window.addText(scope, 325, 555, '0', '#df932e', 'center', 0.5, 0.5, '28px');

        scope.cardLetter1.visible = false;
        scope.cardLetter2.visible = false;
        scope.cardLetter3.visible = false;
        scope.cardLetter4.visible = false;
        scope.cardLetter5.visible = false;
        scope.cardLetter6.visible = false;
        scope.cardNumber1.visible = false;
        scope.cardNumber2.visible = false;
        scope.cardNumber3.visible = false;
        scope.cardNumber4.visible = false;
        scope.cardNumber5.visible = false;
        scope.cardNumber6.visible = false;
    },
    addPuddle: function (scope) {
        scope.anims.create({
            key: 'puddleAnims',
            frames: [
                { key: 'sprite_puddle_of_water0' },
                { key: 'sprite_puddle_of_water1', duration: 50 }
            ],
            frameRate: 2,
            repeat: -1
        });
        scope.puddle = window.addSprite(scope, 400, 300, 'sprite_puddle_of_water0', 0.5, 0.5, 220, 98);
    },
    addFirsInstruction: function (scope) {
        scope.firstInstructionSymbol = window.addSomething(scope, 50, 30, 'alert_symbol', 0.5, 0.5, 38, 38);
        if (scope.state === scope.states.GAME_ENGLISH) {
            scope.firstInstructionText = window.addText(scope, 400, 30, 'Click on a lizard.', '#f8f9f9', 'center', 0.5, 0.5);

        } else if (scope.state === scope.states.GAME_SPANISH) {
            scope.firstInstructionText = window.addText(scope, 400, 30, 'Haz click en una lagartija.', '#f8f9f9', 'center', 0.5, 0.5);
        }
    },
    addSecondInstruction: function (scope) {
        scope.secondInstructionSymbol = window.addSomething(scope, 50, 30, 'ok_symbol', 0.5, 0.5, 38, 38);
        if (scope.state === scope.states.GAME_ENGLISH) {
            scope.secondInstructionText = window.addText(scope, 400, 30, 'Click as many times as necessary on the lizards to find the word.', '#f8f9f9', 'center', 0.5, 0.5);

        } else if (scope.state === scope.states.GAME_SPANISH) {
            scope.secondInstructionText = window.addText(scope, 400, 30, 'Haz click cuantas veces sea necesario\nsobre las lagartijas para encontrar la palabra.', '#f8f9f9', 'center', 0.5, 0.5);
        }
    }
}
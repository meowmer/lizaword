window.WINfunctionality = {
    animsLizards: function (scope) {
        scope.lizardHappy1.play('lizardHappy1Anims');
        scope.lizardHappy2.play('lizardHappy2Anims');
        scope.lizardHappy3.play('lizardHappy3Anims');
        scope.lizardHappy4.play('lizardHappy4Anims');
        scope.lizardHappy5.play('lizardHappy5Anims');
        scope.lizardHappy6.play('lizardHappy6Anims');
    },
    clearGAMEState: function (scope) {
        scope.lizard6.destroy();
        scope.lizard5.destroy();
        scope.lizard4.destroy();
        scope.lizard3.destroy();
        scope.lizard2.destroy();
        scope.lizard1.destroy();

        scope.card6.destroy();
        scope.card5.destroy();
        scope.card4.destroy();
        scope.card3.destroy();
        scope.card2.destroy();
        scope.card1.destroy();

        scope.cardLetter1.destroy();
        scope.cardLetter2.destroy();
        scope.cardLetter3.destroy();
        scope.cardLetter4.destroy();
        scope.cardLetter5.destroy();
        scope.cardLetter6.destroy();
        scope.cardNumber1.destroy();
        scope.cardNumber2.destroy();
        scope.cardNumber3.destroy();
        scope.cardNumber4.destroy();
        scope.cardNumber5.destroy();
        scope.cardNumber6.destroy();

        scope.firstInstructionSymbol.destroy();
        scope.firstInstructionText.destroy();
        scope.secondInstructionSymbol.destroy();
        scope.secondInstructionText.destroy();
    }
}
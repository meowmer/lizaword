// Copy this to use:
// = window.addSomething(scope, setX, setY, idString, setOriginX, setOriginY, setWidth, setHeight);
// = window.addSprite(scope, setX, setY, idString, setOriginX, setOriginY, setWidth, setHeight);
// = window.addText(scope, setX, setY, stringText, color, align, setOriginX, setOriginY);
// = window.addText(scope, 400, 30, 'Awesome text.', '#f8f9f9', 'center', 0.5, 0.5);
window.addSomething = function (scope, setX, setY, idString, setOriginX, setOriginY, setWidth, setHeight) {
    let element = scope.add.image(setX, setY, idString)
    .setOrigin(setOriginX, setOriginY);
    element.displayWidth = setWidth;
    element.displayHeight = setHeight;
    return element;
};
window.addSprite = function (scope, setX, setY, idString, setOriginX, setOriginY, setWidth, setHeight) {
    let element = scope.physics.add.sprite(setX, setY, idString)
    .setOrigin(setOriginX, setOriginY);
    element.displayWidth = setWidth;
    element.displayHeight = setHeight;
    return element;
};
window.addText = function (scope, setX, setY, stringText, color, align, setOriginX, setOriginY, size) {
    let text = scope.add.text(setX, setY, stringText)
        .setColor(color)
        .setAlign(align)
        .setOrigin(setOriginX, setOriginY);
    if (size)
        text.setFontSize(size);
    return text;
}
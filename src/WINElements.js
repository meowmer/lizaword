window.WINElements = {
    createLizardAnims: function (scope, lizaNumber) {
        lizaNumber = lizaNumber.toString(10); // transform number to string
        scope.anims.create({
            key: 'lizardHappy'+ lizaNumber + 'Anims',
            frames: [
                { key: 'sprite_lizard_'+ lizaNumber + '_win0' },
                { key: 'sprite_lizard_'+ lizaNumber + '_win1', duration: 50 }
            ],
            frameRate: 2,
            repeat: -1
        });
    },
    addLizards: function (scope) {
        window.WINElements.createLizardAnims(scope, 6);
        window.WINElements.createLizardAnims(scope, 5);
        window.WINElements.createLizardAnims(scope, 4);
        window.WINElements.createLizardAnims(scope, 3);
        window.WINElements.createLizardAnims(scope, 2);
        window.WINElements.createLizardAnims(scope, 1);
        
        scope.lizardHappy6 = window.addSprite(scope, 400, 430, 'sprite_lizard_6_win0', 0.5, 0.5, 113, 120);
        scope.lizardHappy5 = window.addSprite(scope, 620, 410, 'sprite_lizard_5_win0', 0.5, 0.5, 220, 98);
        scope.lizardHappy4 = window.addSprite(scope, 630, 290, 'sprite_lizard_4_win0', 0.5, 0.5, 190, 83);
        scope.lizardHappy3 = window.addSprite(scope, 400, 175, 'sprite_lizard_3_win0', 0.5, 0.5, 113, 120);
        scope.lizardHappy2 = window.addSprite(scope, 180, 250, 'sprite_lizard_2_win0', 0.5, 0.5, 220, 98);
        scope.lizardHappy1 = window.addSprite(scope, 200, 380, 'sprite_lizard_1_win0', 0.5, 0.5, 190, 83);
    },
    addGamedWord: function (scope) {
        const word = window.WordsGameLogic.gamedWords.pop();
        scope.gamedWord = window.addText(scope, 400, 300, word, '#2a644c', 'center', 0.5, 0.5, '70px');
    }
}
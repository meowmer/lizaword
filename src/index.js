const GameScene = new Phaser.Class({

    Extends: Phaser.Scene,

    initialize:

    function GameScene () {
        Phaser.Scene.call(this, { key: 'gameScene', active: true });
        
        // state SELECTLGE
        this.lizardSELECTLGE = null;
        this.btnEnglishLanguage = null;
        this.btnSpanishLanguage = null;
        this.btnEnglishText = null;
        this.btnSpanishText = null;
        this.gameTitle = null;
        this.cardD4 = null;
        this.cardO2 = null;
        this.cardR3 = null;
        this.cardW1 = null;
        
        // state GAME
        this.firstInstructionText = null;
        this.secondInstructionText = null;
        this.firstInstructionSymbol = null;
        this.secondInstructionSymbol = null;
        this.puddle = null;
        this.lizard1 = null;
        this.lizard2 = null;
        this.lizard3 = null;
        this.lizard4 = null;
        this.lizard5 = null;
        this.lizard6 = null;
        this.card1 = null;
        this.card2 = null;
        this.card3 = null;
        this.card4 = null;
        this.card5 = null;
        this.card6 = null;
        this.cardLetter1 = null;
        this.cardLetter2 = null;
        this.cardLetter3 = null;
        this.cardLetter4 = null;
        this.cardLetter5 = null;
        this.cardLetter6 = null;
        this.cardNumber1 = null;
        this.cardNumber2 = null;
        this.cardNumber3 = null;
        this.cardNumber4 = null;
        this.cardNumber5 = null;
        this.cardNumber6 = null;
        this.currentLetter1 = '';
        this.currentLetter2 = '';
        this.currentLetter3 = '';
        this.currentLetter4 = '';
        this.currentLetter5 = '';
        this.currentLetter6 = '';

        // state WIN
        this.lizardHappy1 = null;
        this.lizardHappy2 = null;
        this.lizardHappy3 = null;
        this.lizardHappy4 = null;
        this.lizardHappy5 = null;
        this.lizardHappy6 = null;
        this.gamedWord = '';

        // Audio
        this.audioStart = null;
        this.audioWin = null;
        this.lizardSound1 = null;
        this.lizardSound2 = null;
        this.lizardSound3 = null;
        this.lizardSound4 = null;

        this.states = {
            SELECTLGE: 1,
            GAME_ENGLISH: 2,
            GAME_SPANISH: 3,
            WIN: 4,
            GAMEOVER: 5
        }
        this.state = this.states.SELECTLGE;
    },

    preload: function () {
        this.load.path = 'src/assets/';
        // state SELECTLGE
        this.load.image('lizard_SELECTLGE', 'Lagartija_selectlge.svg');
        this.load.image('lizaword_title', 'LizaWord_title.svg');
        this.load.image('word_d4', 'word_D4.svg');
        this.load.image('word_o2', 'word_O2.svg');
        this.load.image('word_r3', 'word_R3.svg');
        this.load.image('word_w1', 'word_W1.svg');
        this.load.image('btn_lge', 'btn-idiomas.svg');

        // state GAME
        this.load.image('ok_symbol', 'okSymbol.svg');
        this.load.image('alert_symbol', 'alertSymbol.svg');
        this.load.image('card', 'card.svg');
        this.load.image('sprite_puddle_of_water0', 'sprite_puddle_of_water0.svg');
        this.load.image('sprite_puddle_of_water1', 'sprite_puddle_of_water1.svg');
        this.load.image('sprite_lizard_10', 'sprite_lizard_10.svg');
        this.load.image('sprite_lizard_11', 'sprite_lizard_11.svg');
        this.load.image('sprite_lizard_20', 'sprite_lizard_20.svg');
        this.load.image('sprite_lizard_21', 'sprite_lizard_21.svg');
        this.load.image('sprite_lizard_30', 'sprite_lizard_30.svg');
        this.load.image('sprite_lizard_31', 'sprite_lizard_31.svg');
        this.load.image('sprite_lizard_40', 'sprite_lizard_40.svg');
        this.load.image('sprite_lizard_41', 'sprite_lizard_41.svg');
        this.load.image('sprite_lizard_50', 'sprite_lizard_50.svg');
        this.load.image('sprite_lizard_51', 'sprite_lizard_51.svg');
        this.load.image('sprite_lizard_60', 'sprite_lizard_60.svg');
        this.load.image('sprite_lizard_61', 'sprite_lizard_61.svg');
        this.load.image('sprite_lizard_61', 'sprite_lizard_61.svg');

        // state WIN
        this.load.image('sprite_lizard_1_win0', 'sprite_lizard_1_win0.svg');
        this.load.image('sprite_lizard_1_win1', 'sprite_lizard_1_win1.svg');
        this.load.image('sprite_lizard_2_win0', 'sprite_lizard_2_win0.svg');
        this.load.image('sprite_lizard_2_win1', 'sprite_lizard_2_win1.svg');
        this.load.image('sprite_lizard_3_win0', 'sprite_lizard_3_win0.svg');
        this.load.image('sprite_lizard_3_win1', 'sprite_lizard_3_win1.svg');
        this.load.image('sprite_lizard_4_win0', 'sprite_lizard_4_win0.svg');
        this.load.image('sprite_lizard_4_win1', 'sprite_lizard_4_win1.svg');
        this.load.image('sprite_lizard_5_win0', 'sprite_lizard_5_win0.svg');
        this.load.image('sprite_lizard_5_win1', 'sprite_lizard_5_win1.svg');
        this.load.image('sprite_lizard_6_win0', 'sprite_lizard_6_win0.svg');
        this.load.image('sprite_lizard_6_win1', 'sprite_lizard_6_win1.svg');
        this.load.image('sprite_lizard_6_win1', 'sprite_lizard_6_win1.svg');

        // audio
        this.load.audio('start', 'LizaWord-inicio.wav');
        this.load.audio('youWin', 'LizaWordWin.wav');
        this.load.audio('lizardSound', 'lizardSound.wav');
        this.load.audio('lizardSound2', 'lizardsound2.wav');
        this.load.audio('lizardSound3', 'lizardsound3.wav');
        this.load.audio('lizardSound4', 'lizardsound4.wav');
    },

    create: function () {
        // Audio
        this.audioStart = this.sound.add('start');
        this.audioWin = this.sound.add('youWin');
        this.lizardSound1 = this.sound.add('lizardSound');
        this.lizardSound2 = this.sound.add('lizardSound2');
        this.lizardSound3 = this.sound.add('lizardSound3');
        this.lizardSound4 = this.sound.add('lizardSound4');

        // SELECTLGE state
        window.SELECTLGEElements.playSound(this);
        window.SELECTLGEElements.addGameTitle(this);
        window.SELECTLGEElements.addBtnEnglish(this);
        window.SELECTLGEElements.addBtnSpanish(this);
        window.SELECTLGEElements.addCards(this);
        window.SELECTLGEElements.addLizard(this);

        window.SELECTLGEFunctionality.onBtnEnglish(this);
        window.SELECTLGEFunctionality.onBtnSpanish(this);

        
    },

    update: function () {}
});

const config = {
    type: Phaser.AUTO,
    scale: {
        mode: Phaser.Scale.FIT,
        parent: 'phaser-example',
        autoCenter: Phaser.Scale.CENTER_BOTH,
        width: 800,
        height: 600
    },
    physics: {
        default: 'arcade'
    },
    audio: {
        disableWebAudio: true
    },
    pixelArt: true,
    scene: GameScene
};

const game = new Phaser.Game(config);
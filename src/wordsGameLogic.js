window.WordsGameLogic = {
    wordsSPANISH: [
        'JUNTA', 'UNION', 'AUNAR', 'ALIAR', 'REUNIR', 'QUEDAR', 'SENTIR',
        'ESTAR', 'DARSE', 'BIEN', 'HECHO', 'HACER', 'FAVOR', 'VIVIR'
    ],
    wordsENGLISH: [
        'JOIN', 'FUSION', 'LINK', 'UNITE', 'GATHER', 'MEET', 'FEEL', 'SENSE',
        'GIVE', 'GOOD', 'MAKE', 'DONE', 'PLEASE', 'BEING'
    ],
    choosedWord: '',
    gamedWords: [],
    lizardsLetters: [],
    chooseAWord: function (wordsArray) {
        // choose a number between 0 and wordsArray.length
        // use it like an index to select the word.
        const index = Math.floor(Math.random() * wordsArray.length); // returns a random integer from 0 to wordsArray.length - 1
        // save choosed word
        window.WordsGameLogic.choosedWord = wordsArray[index];
        // store wordsArray[index] in gamedWords array
        window.WordsGameLogic.gamedWords.push(wordsArray[index]);
        // delete wordsArray[index] from wordsArray
        wordsArray.splice(index, 1);
        // return the choosed word to use it to select lizards and groups of letters.
        return window.WordsGameLogic.choosedWord;
    },
    hideLizards: function(scope, choosedWord) {
        // get amount of letters
        const amount = choosedWord.length;
        switch (amount) {
            case 4: // show only lizards 1,2,4,5
                scope.lizard6.visible = false;
                scope.lizard3.visible = false;
                break;
            case 5: // show only lizards 1,2,4,5,6
                scope.lizard3.visible = false;
                break;
            default: // Case 6. Show all lizards
                break;
        }
    },
    createArrayOfLettersPerLetter(scope, choosedWord) {
        // Tip: ASCII Mayus Alphabet: from 65 to 90. Ñ = 165.
        // issue 165 is transformed into ¥

        // identify ASCII number of each letter of choosed word
        for (let i = 0; i < choosedWord.length; i++) {
            const choosedLetter = choosedWord.charCodeAt(i);
            // for each letter, create an array with choosed letter and 4 different letters
            let arrayOfLetters = [];
            // store the choosed letter like a string
            arrayOfLetters.push(String.fromCharCode(choosedLetter));
            let randomLetterIsStored = false;
            while (arrayOfLetters.length < 3) { // EASY 3, HARD 5
                const max = 90;
                const min = 65;
                // Choose a random letter in ASCII code
                let randomLetter = null;
                if (scope.state === scope.states.GAME_ENGLISH) {
                    randomLetter = Math.floor((Math.random() * (max - min + 1)) + min); // returns a random integer from 65 to 90
                } else if (scope.state === scope.states.GAME_SPANISH) {
                    randomLetter = Math.floor((Math.random() * (max - min + 2)) + min); // returns a random integer from 65 to 91
                }
                
                // we have to find if randomLetter is stored in the arrayOfLetters
                // check arrayOfLetters
                for (let i = 0; i < arrayOfLetters.length; i++) {
                    if (randomLetter === (arrayOfLetters[i].charCodeAt(0))) {
                        randomLetterIsStored = true;
                    }
                }
                // if the randomLetter is not stored, we store it
                if (!randomLetterIsStored) {
                    // if the randomLetter code is 91, we store a Ñ
                    if (randomLetter === 91) {
                        randomLetter = 165; // code of Ñ // issue 165 is transformed into ¥
                        arrayOfLetters.push('Ñ');
                    } else {
                        arrayOfLetters.push(String.fromCharCode(randomLetter));
                    }
                }
                randomLetterIsStored = false;
            }
            // put the content of the array in a random order
            arrayOfLetters = window.WordsGameLogic.randomOrderOfArrayOfLetters(arrayOfLetters);

            window.WordsGameLogic.lizardsLetters.push(arrayOfLetters);
        }
    },
    randomOrderOfArrayOfLetters: function (arrayOfLetters) {
        // lista = lista.sort(function() {return Math.random() - 0.5});
        // Fuente: https://www.ngeeks.com/javascript-avanzado-desordenar-un-array/
        arrayOfLetters = arrayOfLetters.sort(function() { return Math.random() - 0.5});
        console.log('arrayOfLetters; ', arrayOfLetters);
        return arrayOfLetters;
    },
    returnALetterFromLizardLettersArray: function (lizardIndex, currentLetter) {
        // Get the array of letters correspondent to the lizard
        const lizardLettersArray = window.WordsGameLogic.lizardsLetters[lizardIndex];
        // If is the first click on the lizard we return the first element of the list
        if (currentLetter === null) {
            return lizardLettersArray[0];
        } else { // we have to find the current letter inside the lizardLettersArray and return the next letter.
            const currentIndex = lizardLettersArray.indexOf(currentLetter);
            if (currentIndex === (lizardLettersArray.length - 1)) {
                return lizardLettersArray[0];
            } else {
                return lizardLettersArray[currentIndex + 1];
            }
        }
    }
}